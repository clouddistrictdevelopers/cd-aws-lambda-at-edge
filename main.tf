/**
 * Source: https://github.com/dod-iac/terraform-aws-lambda-edge-function
 * Some ideas: https://github.com/transcend-io/terraform-aws-lambda-at-edge
 * Creates a Lambda@Edge function to integrate with CloudFront distributions.
 */

data "aws_partition" "current" {}

/**
 * Make a role that AWS services can assume that gives them access to invoke our function.
 */
resource "aws_iam_role" "execution_role" {
  name               = var.execution_role_name
  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "",
        "Action": "sts:AssumeRole",
        "Effect": "Allow",
        "Principal": {
          "Service": [
            "lambda.amazonaws.com",
            "edgelambda.amazonaws.com"
          ]
        }
      }
    ]
  }
  EOF
  tags               = var.tags
}

/**
 * Policy to allow lambda to write logs in CloudWatch.
 */
data "aws_iam_policy_document" "execution_role" {
  statement {
    sid = "AllowCloudWatchLogs"
    actions = [
      "logs:CreateLogGroup",
      "logs:PutLogEvents",
      # Lambda@Edge logs are logged into Log Groups in the region of the edge location
      # that executes the code. Because of this, we need to allow the lambda role to create
      # Log Groups in other regions
      "logs:CreateLogStream"
    ]
    effect = "Allow"
    resources = [
      format(
        "arn:%s:logs:*::log-group:/aws/lambda/*:*:*",
        data.aws_partition.current.partition
      )
    ]
  }
}

resource "aws_iam_policy" "execution_role" {
  name   = var.execution_role_policy_name
  path   = "/"
  policy = length(var.execution_role_policy_document) > 0 ? var.execution_role_policy_document : data.aws_iam_policy_document.execution_role.json
}

resource "aws_iam_role_policy_attachment" "execution_role" {
  role       = aws_iam_role.execution_role.name
  policy_arn = aws_iam_policy.execution_role.arn
}

resource "aws_lambda_function" "main" {
  function_name    = var.function_name
  description      = var.function_description
  filename         = var.filename
  source_code_hash = filebase64sha256(var.filename)
  handler          = var.handler
  runtime          = var.runtime
  role             = aws_iam_role.execution_role.arn
  timeout          = var.timeout
  memory_size      = var.memory_size
  publish          = true
  tags             = var.tags
}

#/**
# * Lambdas are uploaded to via zip files, so we create a zip out of a given directory.
# * In the future, we may want to source our code from an s3 bucket instead of a local zip.
# */
#data archive_file zip_file_for_lambda {
#  type        = "zip"
#  output_path = "lambda_code.zip"
#
#  dynamic source {
#    for_each = distinct(flatten([
#      for blob in var.file_globs :
#      fileset(var.lambda_code_source_dir, blob)
#    ]))
#    content {
#      content = try(
#        file("${var.lambda_code_source_dir}/${source.value}"),
#        filebase64("${var.lambda_code_source_dir}/${source.value}"),
#      )
#      filename = source.value
#    }
#  }
#
#  # Optionally write a `config.json` file if any plaintext params were given
#  dynamic source {
#    for_each = length(keys(var.plaintext_params)) > 0 ? ["true"] : []
#    content {
#      content  = jsonencode(var.plaintext_params)
#      filename = var.config_file_name
#    }
#  }
#}
#
#/**
# * Upload the build artifact zip file to S3.
# *
# * Doing this makes the plans more resiliant, where it won't always
# * appear that the function needs to be updated
# */
#resource aws_s3_bucket_object artifact {
#  bucket                 = var.s3_artifact_bucket
#  key                    = "${var.name}.zip"
#  source                 = data.archive_file.zip_file_for_lambda.output_path
#  etag                   = filemd5(data.archive_file.zip_file_for_lambda.output_path)
#  tags                   = var.tags
#}
#
#/**
# * Create the Lambda function. Each new apply will publish a new version.
# * Should deploy to us-east-1
# */
#resource aws_lambda_function lambda {
#  function_name = var.name
#  description   = var.description
#
#  # Find the file from S3
#  s3_bucket         = var.s3_artifact_bucket
#  s3_key            = aws_s3_bucket_object.artifact.id
#  s3_object_version = aws_s3_bucket_object.artifact.version_id
#
#  publish = true
#  handler = var.handler
#  runtime = var.runtime
#  role    = aws_iam_role.lambda_at_edge.arn
#  tags    = var.tags
#
#  lifecycle {
#    ignore_changes = [
#      last_modified,
#    ]
#  }
#}
#
#
#/**
# * Create the secret SSM parameters that can be fetched and decrypted by the lambda function.
# */
#resource aws_ssm_parameter params {
#  for_each = var.ssm_params
#
#  description = "param ${each.key} for the lambda function ${var.name}"
#
#  name  = each.key
#  value = each.value
#
#  type = "SecureString"
#  tier = length(each.value) > 4096 ? "Advanced" : "Standard"
#
#  tags = var.tags
#}
#
#/**
# * Create an IAM policy document giving access to read and fetch the SSM params
# */
#data aws_iam_policy_document secret_access_policy_doc {
#  count = length(var.ssm_params) > 0 ? 1 : 0
#
#  statement {
#    sid    = "AccessParams"
#    effect = "Allow"
#    actions = [
#      "ssm:GetParameter",
#      "secretsmanager:GetSecretValue",
#    ]
#    resources = [
#      for name, outputs in aws_ssm_parameter.params :
#      outputs.arn
#    ]
#  }
#}
#
#/**
# * Create a policy from the SSM policy document
# */
#resource aws_iam_policy ssm_policy {
#  count = length(var.ssm_params) > 0 ? 1 : 0
#
#  name        = "${var.name}-ssm-policy"
#  description = "Gives the lambda ${var.name} access to params from SSM"
#  policy      = data.aws_iam_policy_document.secret_access_policy_doc[0].json
#}
#
#/**
# * Attach the policy giving SSM param access to the Lambda IAM Role
# */
#resource aws_iam_role_policy_attachment ssm_policy_attachment {
#  count = length(var.ssm_params) > 0 ? 1 : 0
#
#  role       = aws_iam_role.lambda_at_edge.id
#  policy_arn = aws_iam_policy.ssm_policy[0].arn
#}
